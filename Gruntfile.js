'use strict';
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

module.exports = function(grunt) {
  // Load all Grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  grunt.loadNpmTasks('grunt-sftp-deploy');

  grunt.initConfig({
    sass: {
      dist: {
        files: {
          "app/styles/main.css": "app/styles/scss/main.scss"
        }
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({browsers: 'last 2 versions'})
        ]
      },
      dist: {
        src: 'app/styles/main.css'
      }
    },
    cssmin: {
      options: {
        keepBreaks: false,
        keepSpecialComments: 0
      },
      minify: {
        files: [{
          expand: true,
          cwd: 'app/styles/',
          src: ['*.css'],
          dest: 'app/styles/',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
      options: {
        mangle: true,
        sourceMap: true
      },
      build: {
        files: [{
          expand: true,
          src: 'app/scripts/src/{,*/}*.js',
          dest: 'app/scripts/dist',
          flatten: true
        }]
      }
    },
    clean: {
      css: ["app/styles/*.css", "app/styles/*.map"],
      js: ["app/scripts/dist/*.*"]
    },
    copy: {
      lib: {
        expand: true,
        flatten: true,
        files: {
          // JavaScript
          'app/scripts/dist/require.js':      ['app/bower_components/requirejs/require.js']
        }
      },
    },
    watch: {
      css: {
        files: "**/*.scss",
        tasks: ['sass', 'cssmin'],
        options: {
          livereload: true
        }
      },
      js: {
        files: [ "app/scripts/src/**/*.js", "app/config/*.js" ],
        tasks: [ "copy:lib", "uglify" ],
        options: {
          livereload: true
        }
      },
      templates: {
        files: [ "app/templates/*.html" ],
        tasks: [],
        options: {
          livereload: true
        }
      },
      staticContent: {
        files: [ "app/static/*.html" ],
        tasks: [],
        options: {
          livereload: true
        }
      },
      images: {
        files: [ "app/images/**/*.*" ],
        tasks: [],
        options: {
          livereload: true
        }
      }
    },
    open: {
      all: {
        path: 'http://localhost:9000'
      }
    },
    connect: {
      options: {
        port: 9000,
        // change this to '0.0.0.0' to access the server from outside
        //hostname: 'localhost',
        hostname: '0.0.0.0'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              mountFolder(connect, 'app'),
              lrSnippet
            ];
          }
        }
      }
    },
    'sftp-deploy': {
      build: {
        auth: {
          host: 'dev.srrl.matrushka.com.mx',
          port: 22,
          authKey: 'test-ubuntu'
        },
        cache: 'sftpCache.json',
        src: '/Applications/XAMPP/htdocs/srrl-database/client/app',
        dest: '/var/www/dev.srrl.matrushka.com.mx/httpdocs',
        exclusions: ['/client/app/**/.DS_Store', '/client/app/**/Thumbs.db', 'dist/tmp'],
        serverSep: '/',
        concurrency: 4,
        progress: true
      }
    },
    // TODO: include modernizr task
    // modernizr: {
    //   dist: {
    //     "parseFiles": true,
    //     "customTests": [],
    //     "devFile": "node_modules/grunt-modernizr/lib/modernizr-dev.js",
    //     "dest": "app/scripts/dist/modernizr.min.js",
    //     "tests": [
    //       "svg",
    //       "touchevents",
    //       "storage/localstorage"
    //     ],
    //     "extensibility": [
    //       "setClasses"
    //     ],
    //     "uglify": true
    //   }
    // }
  })

  grunt.registerTask('build', function (target) {
    grunt.task.run([
      'clean:css',
      'sass',
      'postcss:dist',
      'cssmin',
      'uglify',
      'copy:lib',
    ]);
  });

  grunt.registerTask('server', function (target) {
    grunt.task.run([
      'build',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server-open', function(target) {
    grunt.task.run(['server', 'open']);
  });

  

}
