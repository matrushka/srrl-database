## Drupal configuration

 - Text filter host and path
 - API token, endpoints, etc

## Tooling

 - Bower
 - Grunt
 - Ruby
 - Sass

## Setting up in DDEV

 - Create configuration files from config_example or copy from server.
 - IN THE client DIRECTORY:
 - Run `npm install`
 - Install Bower dependencies with `bower install`
 - Compile with Grunt `grunt build`

## Map layers

Some of the map layers (labels, background) are hosted in Mapbox, in Noel's account. It should be easy to make new ones in Mapbox studio and replace the old ones if needed. The layers are added in the leafletview.js file.
