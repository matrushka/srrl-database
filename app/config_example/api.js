define(function() {
  var conf = {
    api: {
      host: "sri-test-1.matrushka.com.mx",
      port: 80,
      prefix: '/api/v2',
      environment: 'live',
      requireToken: false
    }
  }
  var a = conf.api
  a.baseURL = 'http://' + a.host + ':' + a.port + a.prefix;

  return conf
})
