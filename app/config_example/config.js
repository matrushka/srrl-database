define([
  'jquery',
  'config/api',
  'config/paths',
  'config/evaluation'
],
function($, api, paths, evaluation) {
  var config = {
    lang: {
      default: 'en'
    },
    taxonomies: ['topics', 'countries', 'evaluation_codes'],
    debug: true
  }

  config = $.extend(config, api, paths, evaluation);;

  // Register globally
  config.register = function(globalName) {
    if(typeof(globalName) == 'undefined') {
      globalName = 'c'
    }

    window[globalName] = this
  }

  return config
})
