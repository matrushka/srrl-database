define({
  evaluation: {
    linear: {
      max: [104, 55, 19],
      mid: [242, 129, 48],
      min: [30, 44, 142]
    },
    codes: {
      marriage: [79, 165, 30],
      unclear: [29, 174, 236],
      neither: [252, 35, 48],
      issue: [253, 104, 172],
      noinfo: [153, 153, 153]
    },
    nocolor: [0, 0, 0]
  }
});
