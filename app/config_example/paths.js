define(function() {
	var config = {
		paths: {
			basePath: '/',
			images: 'images/'
		}
	}

	for(item in config.paths) {
		if(item !== 'basePath') {
			config.paths[item] = config.paths.basePath + config.paths[item]
		}
	}

	return config
})