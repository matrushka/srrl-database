define([
  'config/config',
  'jquery',
  'auth',
  'router',
  'mainview',
  'app_events',
  'taxonomy'
], 

function(C, $, Auth, Router, MainView, App_events, Taxonomy) {

  var App = {
    initialize: function() {
      App.mainView  = new MainView;
      App.auth      = Auth;  

      this.loadTaxonomies(function(err, loaded) {
        if(err) {
          console.error('Error loading taxonomy models');
        }
        else {
          Router.initialize();
        }
      });

      Auth.verify(function(err, data) {
      });
    },
    loadTaxonomies: function(callback) {
      var taxonomies = C.taxonomies;
      var loaded = [];

      App.taxonomies = {};

      taxonomies.forEach(function(item) {
        var taxonomy = new Taxonomy({id: item});
        App.taxonomies[item] = taxonomy;

        taxonomy.load(function(err, model) {
          if(err) {
            callback('Error loading taxonomy model ' + item);
            console.error('Error loading taxonomy model ' + item);
          }
          else {
            loaded.push(item);

            if(taxonomies.length == loaded.length) {
              callback(null, taxonomies);
            }
          }
        })
      });
    },
    register: function(globalName) {
      if(typeof(globalName) == 'undefined') {
        globalName = 'App'
      }

      window[globalName] = this
    }
  };

  $.extend(App, App_events);

  return App;
})
