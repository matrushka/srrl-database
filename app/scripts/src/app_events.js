define({
  onAuth: function() {
    App.mainView.has('auth');
  },
  onLogin: function() {
    this.onAuth();
    App.router.navigate('page/welcome', {trigger: true});
  },
  onAuthFail: function() {
    App.mainView.hasnt('auth');
    App.router.navigate('auth/login', {trigger: true});
  }
});
