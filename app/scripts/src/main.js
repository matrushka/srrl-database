requirejs.config({
  urlArgs: "bust=V1.0.9",
  paths: {
    templates:  '../../templates',
    data: '../../data',
    staticContent:  '../../static',
    languages:  '../../languages',
    config:     '../../config',

    jquery:     '../../bower_components/jquery/dist/jquery.min',
    backbone:   '../../bower_components/backbone/backbone',
    underscore: '../../bower_components/underscore/underscore-min',
    'jquery.cookie': '../../bower_components/jquery-cookie/jquery.cookie',
    underi18n:  '../../bower_components/underi18n/underi18n',
    text:       '../../bower_components/requirejs-text/text',
    select2:    '../../bower_components/select2/dist/js/select2.full.min',
    leaflet:    '../../bower_components/leaflet/dist/leaflet',
    mapbox:     '../../bower_components/mapbox.js/mapbox'
  }
})

require([
  'config/config',
  'app', 
  'templated',
  'language',
  'modernizr-custom',
  'text!data/countries.json'
], 

function(Config, App, Templated, Language, Modernizr) {
  Config.register('C')    // Register the configuration object globally
  Templated.init('$$')    // Register the templating function globally
  Language.register('t')  // Register the translation function globally
  
  App.register()
  App.initialize();
})
