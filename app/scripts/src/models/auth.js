define([
  'jquery',
  'backbone',
  'config/config'
],
function($, Backbone, C) {
  var Auth = Backbone.Model.extend({
    initialize: function() {
      this.url = C.api.baseURL + '/auth';
    },

    // Reads the user credentials from local storage
    // and validates them to set user details if available
    verify: function(callback) {
      if(!C.api.requireToken) {
        App.onAuth();
        return callback(null, {});
      }

      var token = localStorage.getItem('token');

      if(typeof(token) != 'string') {
        App.onAuthFail();
        callback('Token not found');
        return;
      }
      else {
        this.validateToken(token, function(error, data) {
          if(error) {
            App.onAuthFail();
            callback(error);
          }
          else {
            App.onAuth();
            callback(null, data);
          }
        })
      }
    },

    validateToken: function(token, callback) {
      var a = this;

      var settings = {
        url: this.url + '/verify',
        data: {
          token: token
        },
        method: 'POST',
        success: function(data) {
          a.storeToken(data.token);
          a.setUserData(data.user);
          callback(null, data);
        },
        error: function(jqXHR, textStatus, error) {
          callback(error || textStatus);
        }
      };

      $.ajax(settings);
    },

    // Saves a token to localStorage
    storeToken: function(token) {
      localStorage.setItem('token', token);
    },

    // Returns the stored token, if it exists
    getToken: function() {
      return localStorage.getItem('token');
    },

    // Saves user data for use in this session
    setUserData: function(user) {
      for(var prop in user) {
        this.set(prop, user[prop]);
      }
    },

    // Verifies credentials with the API and gets
    // a JWT token and user data
    login: function(mail, password, callback) {
      var a = this;
      var settings = {
        url: this.url + '/login',
        data: {
          'mail': mail,
          'password': password
        },
        method: 'POST',
        success: function(data, status, jqXHR) {
          a.storeToken(data.token);
          a.setUserData(data.user);
          
          App.onLogin();
          return callback(null, data);
        },
        error: function(jqXHR, textStatus, error) {
          App.onAuthFail();

          if(jqXHR.status == 0) {
            return callback('Connection refused');
          }

          return callback(error);
        }
      }

      $.ajax(settings);
    },
    logout: function() {
      localStorage.removeItem('token');
      App.onAuthFail();
    }
  });

  return new Auth;
})
