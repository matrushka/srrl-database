define([
  'backbone',
  'auth'
],

function(Backbone, Auth) {
  var BaseModel = Backbone.Model.extend({
    getCached: function(type, id) {
      return false;
    },
    saveCache: function(type, id, content) {
    },
    fetch: function(options) {
      // Add authentication token (if it exists) to all Ajax calls
      options.beforeSend = function(xhr, settings) {
        var token = Auth.getToken();

        if(token) {
          xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        }
      }

      BaseModel.__super__.fetch.apply(this, arguments)
    }
  });

  return BaseModel;
});
