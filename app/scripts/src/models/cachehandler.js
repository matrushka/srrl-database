define([
  'backbone'
],

function(Backbone) {
  var CacheHandlerModel = Backbone.Model.extend({
    cacheTypes: ['topic', 'country', 'taxonomy'],

    initCache: function() {
      // Start empty cache variables by type if they don't exist
      // Check if the cache should be cleared to make
      // room for newer content
      // If so, clear it.
    },
    getLastAPIUpdateTimestamp: function() {
    },
    getLastStoredUpdateTimestamp: function() {
    }
  });

  return CacheHandlerModel;
});

