define([
  'backbone',
  'config/config',

  'text!templates/contact_message.txt',
  'text!templates/contact_message.html'
],

function(Backbone, C, contact_text_t, contact_html_t) {
  var ContactModel = Backbone.Model.extend({
    url: C.api.baseURL + '/contact/send',
    send: function(fields, callback) {
      // Render the message into plain text and html forms
      var data = {
        mail: fields.email,
        name: fields.name,
        subject: 'National Laws Database ' + ( fields.subject || '' ),
        textMessage: $$({name: 'contact-text', content: contact_text_t}, fields),
        htmlMessage: $$({name: 'contact-html', content: contact_html_t}, fields),
      };

      var settings = {
        url: this.url,
        data: data,
        method: 'POST',
        success: function(data) {
          callback(null, true);
        },
        error: function(jqXHR, textStatus, error) {
          callback(error || textStatus);
        }
      };

      $.ajax(settings);
    }
  });
  return ContactModel;
});
