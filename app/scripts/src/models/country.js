define([
  'config/config',
  'base',
  'text!data/countries.json'
],

function(C, BaseModel, countries_data) {
  var CountryModel = BaseModel.extend({
    initialize: function() {
      this.url = C.api.baseURL + '/country';
    },
    load: function(callback) {
      var options = {
        data: {
          environment: C.api.environment,
          country_id: this.id
        },
        error: function(model, response, options) {
          callback(response);
        },
        success: function(model, response, options) {
          callback(null, model);
        }
      };

      this.fetch(options);

    },
    parse: function(response, options) {
      var country = this;
      var countryData = JSON.parse(response[0].contents);

      // Set country metadata
      this.set('tid', countryData.country_id);
      this.set('name', countryData.doc.country.name);
      this.set('updated', countryData.changed);

      var countryTerm = App.taxonomies.countries.findByTid(countryData.country_id);
      this.set('countryCode', countryTerm.country_code);

      this.set('topics', countryData.doc.children);
    }
  },
  {
    // Static methods
    getCountryNameByCode: function(countryCode) {
      var jsonContents = JSON.parse(countries_data);
      var countries = jsonContents.features;
      var match = _.filter(countries, function(country) {
        return country.id == countryCode;
      });

      return match[0].properties.name;
    },
    getCountryIdByCode: function(countryCode) {
      var match = _.filter(App.taxonomies.countries.get('contents'), function(country) {
        return country.country_code == countryCode;
      })

      return match[0].tid;
    },
    countriesData: countries_data
  });

  return CountryModel;
});
