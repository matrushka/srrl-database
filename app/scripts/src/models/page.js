define([
  'config/config',
  'base'
],

function(C, BaseModel) {
  var PageModel = BaseModel.extend({
    urlRoot: C.api.baseURL + '/page',
    load: function(callback) {
      var options = {
        error: function(model, response, options) {
          callback(response);
        },
        success: function(model, response, options) {
          callback(null, model);
        }
      };

      this.fetch(options);
    },
    parse: function(response, options) {
      var contents = JSON.parse(response.contents);

      for(var prop in contents) {
        this.set(prop, contents[prop]);
      }
    }
  });

  return PageModel;
})
