define([
  'config/config',
  'base'
],

function(C, BaseModel) {
  var TaxonomyModel = BaseModel.extend({
    url: C.api.baseURL + '/taxonomy',
    load: function(callback) {
      if(typeof(this.id) == 'undefined') {
        return callback("Taxonomy ID hasn't been defined");
      }

      var model = this;
      var options = {
        data: {
          taxonomy_name: model.id,
          environment: C.api.environment
        },
        error: function(model, response, options) {
          callback(response);
        },
        success: function(model, response, options) {
          model.set(name, response);
          callback(null, model);
        }
      };

      this.fetch(options);
    },
    parse: function(response, options) {
      if(typeof(response[0]) == 'undefined') {
        console.error('Taxonomy ' + this.id + ' could not be loaded.');
        return false;
      }

      var taxonomy = JSON.parse( response[0].contents );

      if(this.id == 'countries') {
        taxonomy = taxonomy.filter(function(item) {
          return !!parseInt( item.published );
        })
      }

      this.set('contents', taxonomy);
      this.set('loaded', true);
    },
    getContents: function() {
      if(this.get('loaded') !== true) {
        console.error('Attempt to retrieve taxonomy contents before loading data. This method is not asynchronous: getContents()');
        return false;
      }
      else {
        return this.get('contents');
      }
    },
    findByTid: function(tid) {
      var allTerms = this.getContents();

      var term = _.filter(allTerms, function(term) { return term.tid == tid; });

      if(term.length == 0) {
        return false;
      }
      else {
        return term[0];
      }
    }
  });

  return TaxonomyModel;
});
