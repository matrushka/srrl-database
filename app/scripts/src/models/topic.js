define([
  'config/config',
  'base',
  'text!data/countries.json'
],

function(C, BaseModel, countries_data) {
  var TopicModel = BaseModel.extend({
    initialize: function() {
      this.url = C.api.baseURL + '/topic';
    },
    load: function(callback) {
      var options = {
        data: {
          environment: C.api.environment,
          topic_id: this.id
        },
        error: function(model, response, options) {
          callback(response);
        },
        success: function(model, response, options) {
          callback(null, model);
        }
      };

      this.fetch(options);
    },
    parse: function(response, options) {
      var topic = this;

      // Parse countries list
      var countries = {};

      response.forEach(function(item) {
        var country = JSON.parse(item.contents).doc;
        var tid = country.country.tid

        // Add three letter country code
        var countryTerm = App.taxonomies.countries.findByTid(tid);
        country.country.country_code = countryTerm.country_code;

        countries[tid] = country;
      });

      this.set('countries', countries);

      // Add topic term metadata
      var term = App.taxonomies.topics.findByTid(this.id);
      this.set('name', term.name);
      this.set('description', term.description);

      // Add evaluation codes for key
      var evaluationCodesTid = term.evaluation_options;
      var evaluationCodes = App.taxonomies.evaluation_codes.findByTid(evaluationCodesTid).children;
      if(typeof(evaluationCodes) == 'undefined') {
        console.error('Undefined evaluation codes with tid ' + evaluationCodesTid + ' for topic ' + this.id);
      }

      this.set('evaluationCodes', evaluationCodes); 
    },
    getCountryCardByCode: function(countryCode) {
      var countries = this.get('countries');
      var matches   = _.filter(countries, function(country) {
        return country.country.country_code == countryCode;
      })

      if(matches.length == 0) {
        console.warn('No country card found for country code ' + countryCode);
        return false;
      }
      else if (matches.length > 1) {
        console.warn('More than one match found for country code ' + countryCode + '. Returning the first element');
        return matches[0];
      }
      else {
        return matches[0];
      }
    }
  });

  return TopicModel;
});
