define([
  'backbone',
  'scaffold'
], 

function(Backbone, Scaffold) {
  var AppRouter = Backbone.Router.extend({
    routes: {
      '': 'welcome',
      'test': 'testRouter',
      'auth/login': 'login',
      'auth/logout': 'logout',
      'page/:pageName': 'page',
      'map(/:topicId)(/:topicName)(/:countryId)(/:countryName)': 'map',
      'countries(/:countryId)(/:countryName)': 'countries',
      // 'contact(/:subject)': 'contact',
      'grid': 'grid',
      'screen-reader': 'screenReader'
    },
    testRouter: function() {
      Scaffold.test()
    },

    login: function() {
      App.mainView.showLogin();
    },

    logout: function() {
      App.auth.logout();
    },

    page: function(pageName) {
      App.mainView.showPage(pageName);
    },

    map: function(topicId, topicName, countryId, countryName) {
      App.mainView.showMap(topicId, topicName, countryId, countryName);
    },

    countries: function(countryId, countryName) {
      App.mainView.showCountriesList(countryId, countryName);
    },

    // contact: function(subject) {
    //   App.mainView.showContact(subject);
    // },

    welcome: function() {
      App.router.navigate('page/welcome', {trigger: true});
    },

    grid: function(event){
      App.mainView.showGrid();
    },

    screenReader: function() {
      var newLocation = window.location.protocol + '//' + window.location.host + '/static/page-welcome-static.html';
      window.location = newLocation;
    }
  })
  
  var initialize = function() {
    App.router = new AppRouter;
    Backbone.history.start({pushState: true}); 

    $(function (){
      $(document).on("click", "a:not([data-bypass])", function(evt) {
        var href = { prop: $(this).prop("href"), attr: $(this).attr("href") };
        var root = location.protocol + "//" + location.host + Backbone.history.options.root;

        if (href.prop && href.prop.slice(0, root.length) === root) {
          evt.preventDefault();
          ga('set', 'page', '/'+href.attr);
          ga('send', 'pageview','/'+href.attr);
          Backbone.history.navigate(href.attr, true);
        }
      });
    });
  }

  return {initialize: initialize}
})
