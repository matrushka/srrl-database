define([
  'auth',
  'topic',
  'country'
],
function(Auth, Topic, Country) {
  var module = {
    test: function() {
      this.testTopicModel();
    },
    testCountryModel: function() {
      var country = new Country({id: 8});
      country.load(function(err, model) {
        if(err) {
          console.log(err);
        }
        else {
          console.log('We got a country from the server');
          console.log(model);
        }
      });
    },
    testTopicModel: function() {
      var topic = new Topic({id: 21});
      topic.load(function(err, model) {
        if(err) {
          console.log(err);
        }
        else {
          console.log('we got a topic from the server');
          console.log(model);

          var countries = model.get('countries');
          console.log(countries);
          for(var cid in countries) {
            var country = countries[cid];
            console.log('Country: ' + country.country.name);
            console.log('color: ' + country.evaluation.colorStringCSS);
            console.log('code: ' + country.evaluation.text_code);

          }

          console.log('Getting a country card by three-letter country code');
          console.log(model.getCountryCardByCode('RUS'));
        }
      });
    },
    testTopicsCollection: function() {
      var topicsCollection = new TopicsCollection;

      topicsCollection.getList(function(err, list) {
        if(err) {
          console.log('There was an error getting the list');
        }
        else {
          console.log('We got the topics list from the topics collection');
          console.log(list);
        }
      });
    },
    testTaxonomyModel: function() {
      var taxonomyTest = new Taxonomy({id: 'topics'});

      taxonomyTest.load(function(err, data) {
        if(err) {
          console.log(err);
        }
        else {
          console.log(model);
          console.log(taxonomyTest.getContents());
        }
      });
    },
    testLogin: function() {
      var s = this;
      console.log('Do login then test init');
      Auth.login('noel@matrushka.com.mx', 'c3p0', function(err, data) {
        s.testInit();
      });

      console.log('Do logout then test init');
      Auth.logout();
      s.testInit();
    },
    testInit: function() {
      Auth.init(function(err, data) {
        if(err) {
          console.log('Redirect to login page');
          console.log(err);
        }
        else {
          console.log('Show content');
          console.log(data);
        }
      });
    }
  }

  return module;
})
