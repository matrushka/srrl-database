define([
  'backbone',
  'jquery',
  'baseview',

  'text!templates/key.html'
],

function(Backbone, $, BaseView, key_t) {
  var KeyView = BaseView.extend({
    tagName: 'div',
    className: 'classname',

    events: {
    },
    initialize: function() {
      this.render();
    },

    render: function() {
      this.$el.empty().append(key_t);
    }
  });

  return KeyView;
});
