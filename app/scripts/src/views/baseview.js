define([
  'backbone',
  'jquery',
],

function(Backbone, $, key_t) {
  var BaseView = Backbone.View.extend({
    hide: function() {
      this.$el.hide();
    },

    show: function() {
      this.$el.show();
    },

    toggleProperty: function(property) {
      this.$el.toggleClass('has-' + property);
    },

    has: function(property) {
      this.$el.addClass('has-' + property);
    },
    hasnt: function(property) {
      this.$el.removeClass('has-' + property);
    },

    afterRender: function() {
      // Add target blank to document links
      var $documentLinks = this.$el.find('a');
      $documentLinks.each(function() {
        var $link = $(this);
        if($link.data('link-type') == 'document-link') {
          $link.attr('target', 'blank').addClass('document-link');
        }
      });
    }
  });

  return BaseView;
});
