define([
  'jquery',
  'baseview',

  'contact',

  'text!templates/contact.html'
],

function($, BaseView, Contact, contact_t) {
  var ContactView = BaseView.extend({
    tagName: 'div',
    className: 'contact',
    events: {
      'click #contact-submit': 'send',
      'keyup #contact-field-message': 'updateCounter'
    },
    initialize: function() {
      this.maxChars = 1000;
      this.render();
      this.model = new Contact;
    },
    render: function() {
      this.$el.append(contact_t);
      this.$message = this.$el.find('#contact-field-message');
      this.$el.find('.message-counter .count').text(this.maxChars);
    },
    populateSubject: function(subject) {
      var $subject = this.$el.find('#contact-field-subject');
      if(subject) {
        $subject.val('Contribute: ' + subject);
      }
      else {
        $subject.val('');
      }
    },
    send: function(e) {
      var view = this;
      e.preventDefault();
      var values = {};
      var fieldNames = ['name', 'email', 'organization', 'country', 'subject', 'message', 'hitme'];

      fieldNames.forEach(function(fieldName) {
        var value = view.$el.find('#contact-field-' + fieldName).val();
        values[fieldName] = value;
      });

      if(values.hitme != '') {
        view.has('success');
        return;
      }

      if(values.name == '' || values.email == '' || values.message == '') {
        this.validationError();
        return;
      }

      this.model.send(values, function(err, data) {
        if(err) {
          view.scrollToTop();
          view.has('error');
        }
        else {
          view.hasnt('validation-error');
          view.hasnt('error');
          view.has('success');
        }
      });
    },
    validationError: function() {
      this.scrollToTop();
      this.has('validation-error');
    },
    scrollToTop: function() {
      this.$el.find('.fixed-panel').scrollTop(0);
    },
    updateCounter: function(e) {
      var max = this.maxChars;
      var message = this.$message.val();
      var count = message.length;
      var remaining = max - count;
      var $counter = this.$el.find('.message-counter');
      var $submit = this.$el.find('#contact-submit');
      
      if(count >= max) {
        $counter.addClass('too-long');
        $submit.attr('disabled', 'disabled');
      }
      else {
        $counter.removeClass('too-long');
        $submit.removeAttr('disabled');
      }

      $counter.find('.count').text(remaining);

    }
  });

  return ContactView;
})
