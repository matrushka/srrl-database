define([
  'jquery',
  'baseview',

  'text!templates/countries_list.html'
],

function($, BaseView, countries_list_t) {
  var CountriesListView = BaseView.extend({
    tagName: 'div',
    className: 'countries-list',
    events: {
      'click .country-link': 'openCountry'
    },
    initialize: function() {
    },
    render: function() {
      if(this.rendered) return;

      var listData = {
        countries: App.taxonomies.countries.get('contents')
      };

      this.$el.empty().append($$({
        name: 'countries-list',
        content: countries_list_t
      }, listData));

      this.rendered = true;
    },
    openCountry: function(e) {
      var countryId = $(e.target).parents('.country-link').data('country-id');
      this.parent.openCountry(countryId);
    }

  });

  return CountriesListView;
});
