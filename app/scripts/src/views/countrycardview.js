define([
  'backbone',
  'jquery',
  'baseview',
  'country',

  'text!templates/country_card.html'
],

function(Backbone, $, BaseView, Country, country_card_t) {
  var CountryCardView = BaseView.extend({
    tagName: 'div',
    className: 'map__country-card',

    events: {
      'click': 'hide',
      'click .card-container': 'doNothing',
      'click button.close': 'hide',
      'click button.details': 'openCountryDetails'
    },
    doNothing: function(evt) {
      evt.stopPropagation();
    },
    showCountry: function(countryCode) {
      this.currentCountryCode = countryCode;

      var cardData = this.parent.currentTopic.getCountryCardByCode(countryCode);

      if(cardData == false) {
        cardData = {
          noData: true,
        };
      }

      cardData.countryName = Country.getCountryNameByCode(countryCode); 
      cardData.topicName = this.parent.currentTopic.get('name');
      cardData.topicDescription = this.parent.currentTopic.get('description');
      cardData.currentTopic = this.parent.selects.getCurrentTopic();

      ga('send', 'event', {
        'eventCategory': 'Country card '+ cardData.countryName + ' - ' + cardData.topicName,
        'eventAction': 'Click'
      });
      var contents = $$({
        name: 'country-card',
        content: country_card_t
      }, cardData);

      this.$el.empty().append(contents);

      this.show();
    },
    show: function() {
      // this line is necessary to force the browser to actually render the item
      // before executing the CSS transition. If you remove it, instead of having
      // a nice transition, the card will just pop up on screen. Ugly.
      var width = this.$el.width();

      this.$el.addClass('open');
      this.parent.has('country-card');
    },
    hide: function(evt) {
      this.$el.removeClass('open');
      this.parent.hasnt('country-card');
      this.parent.clearCountry();
    },
    openCountryDetails: function() {
      var countryId = Country.getCountryIdByCode(this.currentCountryCode);
      this.parent.openCountry(countryId);
    }
  });

  return CountryCardView;
});
