      globalCounter = 0;

define([
  'backbone',
  'jquery',
  'country',
  'topic',
  'baseview',

  'text!templates/country.html',
  'text!templates/country_topic.html',
  'text!templates/topic_question.html',
  'text!templates/country_topic_select.html'
],

function(Backbone, $, Country, Topic, BaseView, country_t, country_topic_t, topic_question_t, country_topic_select_t) {
  var CountryView = BaseView.extend({
    tagName: 'div',
    className: 'country',

    events: {
      'click span.select2' : 'toggleArrow',
      'click button.go-back': 'hide',
      'click button.crl-only': 'toggleCrl',
      'change select.country-topic-select': 'goToTopic',
      'click button.print': 'print',
      'click div.contribute' : 'goToContact'
    },
    initialize: function() {
      this.countries = {};
      this.crlOnly = false;
      this.topOffset = 130;
      this.select2Options = {
        width: 200,
        containerCssClass: 'srrl',
        dropdownCssClass: 'srrl'
      }
    },

    resetDefaults: function() {
      this.crlOnly = false;
      this.hasnt('crl-only');
    },

    loadCountry: function(countryId) {
      this.resetDefaults();
      var view = this;
      if(typeof(this.countries[countryId]) == 'undefined') {
        var country = new Country({id: countryId});
        country.load(function(err, model) {
          if(err) {
            console.error(err);
          }
          else {
            view.countries[countryId] = model;
            view.currentCountry = model;

            view.renderCountry();
            view.setRoute();
            view.show();
          }
        });
      }
      else {
        view.currentCountry = this.countries[countryId];

        view.renderCountry();
        view.setRoute();
        view.show();
      }
    },

    renderCountry: function() {
      // Get data
      var name = this.currentCountry.get('name');
      var id = this.currentCountry.get('id');
      var term = App.taxonomies.countries.findByTid(id);
      var date = new Date(this.currentCountry.get('updated') * 1000);
      var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'];
      var updateString = date.getFullYear() + '-' + months[date.getMonth()] + '-' + date.getDate();

      var select = this.renderTopicSelect();
      var topics = this.renderTopicsTree();

      var countryData = {
        intro: term.description,
        countryModel: this.currentCountry,
        name: this.currentCountry.get('name'),
        id: this.currentCountry.get('id'),
        updateString: updateString,
        topicsSelect: select,
        topics: topics
      };

      var content = $$({
        name: 'country',
        content: country_t
      }, countryData);

      this.$el.empty().append(content);

      // Setup jump menu with filters
      this.$el.find('.country-topic-select').select2(this.select2Options);

      // Jump to mapView selected topic, if there is one
      if(App.mainView.showing == 'map' && typeof(this.mapView.currentTopic) != 'undefined') {
        var topicId = this.mapView.currentTopic.get('id');
        this.$el.find('.country-topic-select').val(topicId).trigger('change');
      }

      else if(App.mainView.showing == 'grid' && typeof(this.gridView.currentTopic) != 'undefined') {
        var topicId = this.gridView.currentTopic['id'];
        this.$el.find('.country-topic-select').val(topicId).trigger('change');
      }

      this.afterRender();
    },

    renderTopicSelect: function() {
      var view = this;
      var topics = this.currentCountry.get('topics');
      var topicsList = _.map(topics, function(topic) {
        if(!topic.not_covered_in_research) {
          var crlTag = _.find(topic.doc.tags, function(tag) {
            return tag.code == 'CRL';
          });
        }
        var crl = typeof(crlTag) != 'undefined';

        var simpleTopic = {
          name: topic.name,
          id: topic.topic_id,
          crl: crl
        };

        if((view.crlOnly && crl) || !view.crlOnly) {
          return simpleTopic;
        }
      })

      var select = $$({
        name: 'country-topic-select',
        content: country_topic_select_t
      }, { topicsList: topicsList });

      return select;
    },

    renderTopicsTree: function() {
      var countryName = this.currentCountry.get('name');
      var topics = this.currentCountry.get('topics');
      var rendered = [];
      
      topics.forEach(function(topic) {
        if(!topic.not_covered_in_research) {
          var crlTag = _.find(topic.doc.tags, function(tag) {
            return tag.code == 'CRL';
          });
        }
        var crl = typeof(crlTag) != 'undefined';

        var topicData = {
          nid: topic.nid,
          name: topic.name,
          id: topic.topic_id,
          description: topic.description,
          crl: crl,
          textCode: topic.doc.evaluation.text_code,
          countryName: countryName
        };
        topicData.tags = (topic.not_covered_in_research || typeof(topic.not_covered_in_research) == 'undefined') ? topic.doc.tags : [];
        topicData.color = (topic.not_covered_in_research) ? '' : topic.doc.evaluation.color;

        topicData.questions = [];
        if(topic.questions) {
          topic.questions.forEach(function(question) {
            var questionData = {
              nid: question.nid,
              question: question.doc.topic.name,
              explanation: question.doc.explanation,
              answer: question.doc.answer,
              documents: question.doc.related_documents
            };

            var renderedQuestion = $$({
              name: 'topic-question',
              content: topic_question_t
            }, questionData);

            topicData.questions.push(renderedQuestion);
          });
        }

        rendered.push($$({
          name: 'country-topic',
          content: country_topic_t
        }, topicData));
      });
      
      return rendered;
    },

    toggleCrl: function() {
      this.crlOnly = !this.crlOnly;

      if(this.crlOnly) {
        this.has('crl-only');
      }
      else {
        this.hasnt('crl-only');
      }

      var select = this.renderTopicSelect();
      this.$el.find('.topics-select-container').empty().append(select);
      this.$el.find('.country-topic-select').select2(this.select2Options);
    },

    setRoute: function() {
      var name = this.currentCountry.get('name');
      var id = this.currentCountry.get('id');
      
      if(App.mainView.showing == 'map') {
        if(typeof(this.mapView.currentTopic) != 'undefined') {
          var topic = this.mapView.currentTopic;
          var topicName = topic.get('name');
          var topicId = topic.get('id');
        }
          var routerArgs = [topicId || '_', escape(topicName) || '_', id, escape(name)];
          App.router.navigate('map/' + routerArgs.join('/')), {trigger: false};
      }
      else {
        App.router.navigate('countries/' + id + '/' + escape(name));
      }
    },

    goToTopic: function(e) {
      var topicId = this.$el.find('.country-topic-select').val();
      var $content = this.$el.find('.content');

      if(topicId == -1) {
        $content.stop().animate({scrollTop: 0}, 500);
      }
      else {
        // Find the topic html element
        var $topic = this.$el.find('#topic-' + topicId);
        lastTopic = $topic;
        
        var topicPosition = $topic.position();
        var headerheight = this.$el.find('.header').height();
        $content.stop().animate({scrollTop: topicPosition.top - headerheight}, 500);
      }
    },

    show: function() {
      this.parent.has('panel');
    },
    hide: function() {
      this.parent.hasnt('panel');

      if(App.mainView.showing == 'map') {
        this.mapView.setRoute();
      }
      else if(App.mainView.showing == 'grid'){
        this.gridView.setRoute();
      }
      else {
        App.router.navigate('countries', true);
      }
    },
    print: function() {
      window.print();
    },
    goToContact: function(){
      var path = 'contact/' + escape(this.currentCountry.get('name'));
      App.router.navigate(path, {trigger: true});
    },
    toggleArrow: function(evt){
      var selectId = evt.target.id;
      this.$el.find('#'+selectId).next().toggleClass('no-radius');
    }
  });

  return CountryView;
});
