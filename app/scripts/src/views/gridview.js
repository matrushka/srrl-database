define([
  'backbone',
  'jquery',
  'baseview',
  'text!templates/grid.html',
  'topic'
],

function(Backbone, $, BaseView, Grid, Topic) {
  var GridView = BaseView.extend({
    tagName: 'div',
    className: 'grid',

    events: {
      'click span.select2' : 'toggleArrow',
      'change .country-select' : 'showCountry',
      'click .close-button' : 'hideCountry',
      'click .close-all' : 'hideAllCountries',
      'click .country-cell' : 'openCountry'
    },

    initialize: function() {
      this.topicsLoaded = 0;
      this.pendingTopics = 0;
      this.loadedTopics = {};
    },

    showCountry: function(evt) {
      var countryCode = this.$el.find('.country-select').val();
      countryCode = ".country-"+countryCode;
      $(countryCode).removeClass('hidden');
    },

    hideCountry: function(evt){
      var $closeButton = $(evt.target);
      var countryCode = $closeButton.data('country');
      countryCode = ".country-"+countryCode;
      $(countryCode).addClass('hidden');
    },

    hideAllCountries: function(evt) {
      this.$el.find('.country-select').val('-1');
      this.$el.find('.country-select').change();
      this.$el.find('.country-cell, .country-name').addClass('hidden');
    },

    render: function(values) {
      if(this.rendered) {
        return false;
      }
      this.$el.append($$({
      name: 'grid_table',
      content: Grid,
      },values)); 

      var select2Options = {width: '220', dropdownCssClass : "srrl", containerCssClass: "srrl" };
      $('.country-select').select2(select2Options); 
      this.rendered = true;
    },

    loadData: function(){
      var view = this;

      this.countries = App.taxonomies.countries.getContents();
      this.topics = App.taxonomies.topics.getContents();

      this.topics.forEach(function(topic) {
        var should_load = !parseInt(topic.not_covered_in_research);
        if(should_load) {
          view.loadTopic(topic['tid']);
        }
      })
    },

    onTopicLoaded: function(topicId, topicModel) {
      this.loadedTopics[topicId] = topicModel;
      this.topicsLoaded++;

      if(this.pendingTopics == this.topicsLoaded) {
        this.onTopicsLoadEnd();
      }
    },

    onTopicsLoadEnd: function() {
      this.loadedTopics = _.sortBy(this.loadedTopics, function(o) { return o.attributes.name; });
      for(topicIndex in this.loadedTopics) {
        var topic = this.loadedTopics[topicIndex];
        var countries = topic.get('countries');
        countries = _.sortBy(countries, function(o) { return o.country.name; });
      }
      this.values = {countries: this.countries, loadedTopics: this.loadedTopics};
      this.render(this.values);
    },

    openCountry: function(evt) {
      var $countryCell = $(evt.target);
      var countryId = $countryCell.attr('value');
      var topicName = $countryCell.data('topic-name');
      var topicId = $countryCell.data('topic-id');
      this.currentTopic = {name : topicName, id: topicId};
      this.parent.openCountry(countryId);
    },

    setRoute: function() {
      App.router.navigate('grid');
    },

    loadTopic: function(topicId) {
      this.pendingTopics++;

      var countries = {};
      var grid = this;
      if(typeof(this.loadedTopics[topicId]) == 'undefined') {
        var topic = new Topic({id: topicId});
        topic.load(function(err, model) {
          if(err) {
            console.error(err);
          }
          else {
            countries = model.get('countries');
            grid.onTopicLoaded(topicId, model);
          }
        });
      }
      else {
        grid.currentTopic = this.topics[topicId];
      }
    },
    toggleArrow: function(evt){
      var selectId = evt.target.id;
      this.$el.find('#'+selectId).next().toggleClass('no-radius');
    }
  });

  return GridView;
});
