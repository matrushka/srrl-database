define([
  'backbone',
  'jquery',
  'baseview',
  'leafletview',

  'text!templates/key.html'
],

function(Backbone, $, BaseView, LeafletView, key_t) {
  var KeyView = BaseView.extend({
    tagName: 'div',
    className: 'map__key',

    events: {
      'click button.collapse': 'toggleCollapse',
      'click h4.topic-name': 'toggleCollapse',
      'mouseleave': 'collapse',
      'mouseenter': 'expand',
      'click tr.clickable' :'showCountryList',
      'mouseenter tr' : 'highlightArrow',
      'mouseleave tr' : 'highlightArrow'
    },
    initialize: function() {
      var view = this;
      $(window).on('orientationchange', this.setKeyHeight);
      this.showCountries = true;
    },

    render: function() {
      var totalCountries = this.parent.leaflet.totalCountries;
      for(colorKey in totalCountries) {
        totalCountries[colorKey].sort();

      }
      var keyData = {
        evaluationCodes: this.parent.currentTopic.get('evaluationCodes'),
        topicName: this.parent.currentTopic.get('name'),
        topicDescription: this.parent.currentTopic.get('description'),
        totalCountries : totalCountries
      }
      var contents = $$({
        name: 'key',
        content: key_t
      }, keyData);
      this.$el.empty().append(contents);
      
      this.$el.width();
      this.show();
      this.setKeyHeight();
      this.$el.addClass('collapsed');
    },
    toggleCollapse: function() {
      $(".active-label .icon").toggleClass('hidden');
      this.$el.toggleClass('collapsed');
    },
    collapse: function() {
      this.toggleCollapse();
      this.$el.addClass('collapsed');
    },
    expand: function() {
      this.toggleCollapse();
      this.$el.removeClass('collapsed');
    },
    showCountryList: function(evt){
      var $topicName = $(evt.currentTarget);
      var colorId = $topicName[0].id;
      if(this.showCountries){
        $("#"+colorId+' .answer-countries-list').removeClass('hidden');
        $("#"+colorId+' .active-label').addClass('selected');
        $("#"+colorId+' .icon').addClass('rotate-icon');
        $("#"+colorId+' .icon').addClass('light');
        this.showCountries = false;
      }
      else{
        $("#"+colorId+' .answer-countries-list').addClass('hidden');
        $("#"+colorId+' .active-label').removeClass('selected');
        $("#"+colorId+' .icon').removeClass('rotate-icon');
        $("#"+colorId+' .icon').removeClass('rlight');
        this.showCountries = true;
      }
    },
    setKeyHeight: function(){
      var $selects = $(".map__selects").height();
      var $leafletFooter = $(".leaflet-control-attribution").height();
      var $windowHeight = $(window).height();
      var $bottomLine = $("#bottom-line").height();
      var $topicName = $(".topic-name").height();
      $(".table-container").css({maxHeight: $windowHeight - $selects - $leafletFooter - $bottomLine - $topicName -40+ "px"});
    },
    highlightArrow: function(evt){
      var $topicName = $(evt.currentTarget);
      var colorId = $topicName[0].id;
       $("#"+colorId+' .icon').toggleClass('light');
    }
  });

  return KeyView;
});
