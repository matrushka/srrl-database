define([
  'jquery',
  'baseview',
  'leaflet',
  'text!data/countries.json',
  'underscore'
],

function($, BaseView, mapbox, countries_data,_) {
  var LeafletView = BaseView.extend({
    tagName: 'div',
    className: 'map__leaflet',
    events: {
    },

    initialize: function() {
      var view = this;

      this.mapboxToken = 'pk.eyJ1IjoibmVsb3Zpc2hrIiwiYSI6ImNpZzJqeng1eDFlM3J0dmx1Mm4yaHo2MXQifQ.l-5cMYg7-m7NKOjuqIjiJQ';
      this.countries = JSON.parse(countries_data);
      this.southWestMax = [110,220];
      this.northEastMax = [-110,-220];
      this.maxBounds = L.latLngBounds(this.southWestMax, this.northEastMax);
      this.minZoom = 1;
      this.totalCountries = {};

      $(window).resize(function() {
        view.setMapSize();
        view.invalidateSize();
      });
    }, //end of initialize


    render: function() {
      this.$el.append('<div id="map"></div>');
      this.initializeMap();
    },//end of render function

    initializeMap : function() {
      var view = this;
      this.defaultZoom = 2;
      this.setMapSize();

      if ($(window).width() < 767 && $(window).width() > 320) { 
        this.defaultZoom = 0;
      }
      this.map = L.map('map').setView([6, 1], this.defaultZoom);
      this.map.options.minZoom = this.minZoom;
      this.map.setMaxBounds(this.maxBounds);
      this.map.zoomControl.setPosition('bottomleft');

      // this.addBackgroundLayer();
      this.addCountries();
      this.addLabelsLayer();
    },//end of initializeMap

    resetZoom: function() {
      this.map.setView([6,1], this.defaultZoom);
    },

    setMapSize: function() {
      // Set the map size
      var topLineHeight = $('#app-container > #top-line').height();
      var bottomLineHeight = $('#app-container > #bottom-line').height();
      var selectsHeight = $('.map__selects').height();
      var windowHeight = $(window).height();

      var mapHeight = windowHeight - selectsHeight - bottomLineHeight;
      this.$el.find('#map').height(mapHeight);
    },

    addBackgroundLayer: function() {
      var url = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=' + this.mapboxToken;
      var background = L.tileLayer(url, {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 6,
        id: 'nelovishk/ck8gj5qmd0aei1imphi0lkx3s',
        accessToken: this.mapboxToken,
        continuousWorld: false,
        noWrap: true
      }).addTo(this.map);
    },

    addLabelsLayer: function() {
      var url = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=' + this.mapboxToken;
      var background = L.tileLayer(url, {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 6,
        id: 'nelovishk/clar4vwi3000714p2k0e3d0m2',
        accessToken: this.mapboxToken,
        continuousWorld: false,
        noWrap: true
      }).addTo(this.map);
    },

    addCountries: function() {
      var view = this;
      this.countriesLayer = L.geoJson(this.countries, {
        style: view.getPolygonDefaultStyle(),
        onEachFeature: view.initializeCountryPolygon,
        mapView: view
      });
      this.map.addLayer(this.countriesLayer);
    },
  
    highlightFeature: function(e) {
      // TODO: highlight without messing visualization up.
      return; 

      var layer = e.target;
        layer.setStyle({
          weight: 2,
          color: 'rgb(150, 150, 150)',
        });
      if (!L.Browser.ie && !L.Browser.opera) {
        layer.bringToFront();
      }
    },

    colorCountries: function (topicCountries) { //color country when topic selected
      var view = this;
      var newCountries = this.countries;
      var colors = {};
      this.totalCountries = {};

      for(country in topicCountries) {
        colors[topicCountries[country]['country']['country_code']] = topicCountries[country]['evaluation']['color'];
      }
      
      var num = 0;
      var layerColor;
      var countryNumbers = {};
      var cont =0;
      var countryNames = {};
      for(countryId in this.countries.features){
        countryNames[this.countries['features'][countryId]['id']] = this.countries['features'][countryId]['properties']['name'];
      }
      this.countriesLayer.eachLayer(function(layer) {
        layerColor = colors[ layer.feature.id ];
        if(typeof(layerColor) != 'undefined') {
          layer.setStyle({
            fillColor: layerColor
          });
        }
        else {
          view.countriesLayer.resetStyle(layer);
        }
      });

      var colorGroups = {};
      for(country in colors) {
        if(typeof(this.totalCountries[colors[country]]) == 'undefined') {
          this.totalCountries[colors[country]] = [];
        }
        if(typeof(countryNames[country]) != 'undefined') {
          this.totalCountries[colors[country]].push(countryNames[country]);
        }
      }
    },

    resetColors: function() {
      var view = this;
      this.countriesLayer.eachLayer(function(layer) {
        view.countriesLayer.resetStyle(layer);
      });
    },

    zoomToFeature: function (e)  { 
      // TODO: fix the awkwardness. Something is strange with the zoom.
      return; 
      var offsetLng = 0;
      var southWest = [e.target.getBounds()._southWest.lat, e.target.getBounds()._southWest.lng];
      var northEast = [e.target.getBounds()._northEast.lat, e.target.getBounds()._northEast.lng];
      var bounds = L.latLngBounds(southWest, northEast);
      switch(this.map.getBoundsZoom(bounds)) {
        case 1:
          offsetLng =30;
        break;
        case 2:
         offsetLng = 60;
        break;
        case 3:
         offsetLng = 25;
        break;
        case 4:
         offsetLng = 8;
        break;
        case 5:
         offsetLng = 6;
        break;
        case 6:
         offsetLng = 3;
        break;
        case 7:
          offsetLng = 0.5;
        break;
      }

      southWest = [e.target.getBounds()._southWest.lat, e.target.getBounds()._southWest.lng+offsetLng];
      northEast = [e.target.getBounds()._northEast.lat, e.target.getBounds()._northEast.lng+offsetLng];
      bounds = L.latLngBounds(southWest, northEast);
      this.map.fitBounds(bounds);
    },

    getPolygonDefaultStyle: function() {
      return {
          fillColor: '#d3d3d3',
          weight: 1,
          opacity: 1,
          color: '#ffffff',
          dashArray: '1',
          fillOpacity: 1
      };
    },

    initializeCountryPolygon: function(feature, Polygon) {
      var view = this.mapView;
      if (feature.properties)  { 
        // Bind the click event
        Polygon.on('click', function (e) {
          // If no topic has been selected, stop
          if(typeof(view.parent.currentTopic) == 'undefined') {
            return;
          }

          if( typeof(view.lastPolygonClickEvent) != 'undefined' ) {
            // TODO: highlight without messing up visualization
            // view.countriesLayer.resetStyle(view.lastPolygonClickEvent.target);
          }

          view.zoomToFeature(e);
          view.highlightFeature(e);
          view.parent.highlightCountry(e.target.feature.id);
          view.lastPolygonClickEvent = e;

          // Bind the onzoomend event to fix the position for
          // specific countries
          // TODO: fix this, it's not working properly
          // disabling by now
          
          // var centerCountry = function()  {
          //   var featureId = e.target.feature.id;
          //   var center = view.getCountryCenter(featureId);
          //   if(center) {
          //     view.map.setView(center);
          //   }

          //   view.map.off('zoomend', centerCountry);
          // };

          // view.map.on('zoomend', centerCountry);

        });
      }
    },

    getCountryCenter: function(featureId) {
      var center;

      switch(featureId)
      {
        case 'RUS':
          center = [60,90];
          break;
        case 'USA':
          center = [41,-116];
          break;
        case 'CAN':
          center = [60,-115];
          break;
        case 'NOR':
          center = [61,5];
          break;
        case 'DOM':
          center = [18.78785,-71.32324];
          break;
        case 'HTI':
          center = [19.07883,-72.20215];
          break;
        default:
          center = false;
          break;
      }

      return center;
    },
    
    // This is required to force Leaflet to render since it was
    // hidden before (display: none)
    invalidateSize: function() {
      if(this.map) {
        this.map.invalidateSize();
      }
    }

  });

  return LeafletView;
});
