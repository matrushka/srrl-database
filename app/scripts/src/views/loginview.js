define([
  'backbone',
  'jquery',
  'baseview',

  'auth',

  'text!templates/login.html'
],

function(Backbone, $, BaseView, Auth, login_t) {
  var LoginView = BaseView.extend({
    tagName: 'form',
    id: 'login',

    events: {
      'click #login-submit': 'doLogin'
    },

    initialize: function() {
      this.render();
    },

    render: function() {
      this.$el.append(login_t);
      this.$form = this.$el.find('#login');
    },

    doLogin: function(evt) {
      var loginView = this;
      evt.preventDefault();

      // Get the mail and password values
      var mail      = this.$el.find('#login-mail').val();
      var password  = this.$el.find('#login-password').val();
      
      // STATE consider while resetting
      this.$form.addClass('loading');

      Auth.login(mail, password, function(err, data) {
        if(err) {
          loginView.$form.removeClass('loading');

          // STATE consider while resetting
          loginView.$form.addClass('with-error');
        }
      });
    },

    reset: function() {
      this.$form.removeClass('loading');
      this.$form.removeClass('with-error');
    }
  });

  return LoginView;
});
