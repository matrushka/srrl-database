define([
  'backbone',
  'jquery',
  'baseview', 

  'loginview',
  'menuview',
  'pageview',
  'mapview',
  'countryview',
  'countrieslistview',
  'contactview',
  'gridview',

  'text!templates/main.html',
], 

function(Backbone, $, BaseView, LoginView, MenuView, PageView, MapView, CountryView, CountriesListView, ContactView, GridView, main_t) {
  var MainView = BaseView.extend({
    el: $('#app-container'),

    events: {
      'click #menu-link': 'toggleMenu'
    },

    initialize: function() {
      // Initialize child views
      this.menuView = new MenuView;
      this.menuView.parent = this;
      this.loginView = new LoginView;
      this.pageView = new PageView;
      this.mapView = new MapView;
      this.countriesListView = new CountriesListView;
      this.contactView = new ContactView;
      this.gridView = new GridView;

      this.countryView = new CountryView;
      this.countryView.parent = this;
      this.countryView.mapView = this.mapView;
      this.countryView.gridView = this.gridView;
      this.mapView.countryView = this.countryView;


      this.countriesListView.parent = this;
      this.mapView.parent = this;
      this.gridView.parent = this;

      this.contentViews = [this.loginView, this.pageView, this.mapView, this.countriesListView, this.contactView, this.gridView];

      this.render();
    },

    render: function() {
      this.$el.empty().append(main_t);
      
      this.$menuContainer   = this.$el.find('#menu');
      this.$contentWrapper  = this.$el.find('#content-wrapper');
      this.$rightPanel      = this.$el.find('#right-panel');

      this.$menuContainer.append(this.menuView.$el);
      this.$contentWrapper.append(this.loginView.$el);
      this.$contentWrapper.append(this.pageView.$el);
      this.$contentWrapper.append(this.mapView.$el);
      this.$contentWrapper.append(this.countriesListView.$el);
      this.$contentWrapper.append(this.contactView.$el);
      this.$contentWrapper.append(this.gridView.$el);
      this.$rightPanel.append(this.countryView.$el);

      this.hideAllContent();
    },

    hideAllContent: function() {
      this.contentViews.forEach(function(view) {
        view.hide();
      });
      this.hasnt('panel');
    },

    showLogin: function() {
      var mainView = this;
      App.auth.verify(function(err, data) {
        if(err) {
          mainView.loginView.reset();
          mainView.loginView.show();
        }
        else {
          App.router.navigate('page/welcome');
        }
      })
    },

    showPage: function(pageName) {
      this.showing = 'page';

      this.hideAllContent();
      this.pageView.show();
      this.pageView.showPage(pageName);
    },

    showMap: function(topicId, topicName, countryId, countryName) {
      this.showing = 'map';

      this.hideAllContent();
      this.mapView.render();
      this.mapView.show();

      if(!isNaN(parseInt(topicId))) {
        this.mapView.loadTopic(topicId);
      }
      if(!isNaN(parseInt(countryId))) {
        this.mapView.openCountry(countryId);
      }
    },

    showCountriesList: function(countryId) {
      this.showing = 'countries-list';
      this.hideAllContent();
      this.countriesListView.render();
      this.countriesListView.show();

      if(!isNaN(parseInt(countryId))) {
        this.openCountry(countryId);
      }
    },

    showContact: function(subject) {
      this.showing = 'contact';
      this.hideAllContent();
      this.contactView.show();
      this.contactView.populateSubject(subject);
    },

    openCountry: function(countryId) {
      this.countryView.loadCountry(countryId);
      this.closeMenu();
    },

    toggleMenu: function() {
      this.toggleProperty('menu');
    },

    showGrid: function(){
      this.showing = 'grid';
      this.hideAllContent();
      this.gridView.loadData();
      this.gridView.show();
    },

    closeMenu: function() {
      this.hasnt('menu');
    }
  })

  return MainView
})
