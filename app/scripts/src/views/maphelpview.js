define([
  'backbone',
  'jquery',
  'baseview',

  'text!templates/map_help.html'
],

function(Backbone, $, BaseView, map_help_t) {
  var MapHelpView = BaseView.extend({
    tagName: 'div',
    className: 'map__help',

    events: {
    },
    initialize: function() {
    },

    render: function() {
      this.$el.empty().append(map_help_t);
    }

  });

  return MapHelpView;
});
