define([
  'backbone',
  'jquery',
  'baseview',
  'text!templates/selects.html',
  'select2',
],

function(Backbone, $, BaseView, selects_t) {
  var SelectsView = BaseView.extend({
    tagName: 'div',
    className: 'map__selects',

    events: {
      'click span.select2' : 'toggleArrow',
      'change .country-select' : 'showCountry',
      'change .topic-select' : 'showTopic',
      'click span.close' : 'closeModal',
      'click button.clear': 'clearMap'
    },

    initialize: function() {
      this.select2Options = {width: '220', dropdownCssClass : "srrl", containerCssClass: "srrl" };
      this.notCoveredTopics = [];
    },

    hideToolTip: function(){
      var $select = this.$el.find('.topic-select');
      var topicId = $select.find('option:selected').val();
      var $tooltip = this.$el.find('.tooltip .tooltiptext');
      if(topicId!=-1)
        $tooltip.css('visibility','hidden');
      else
        $tooltip.css('visibility','visible');
    },

    showCountry: function(evt) {
      var countryCode = this.$el.find('.country-select').val();
      this.parent.highlightCountry(countryCode);
    },

    showTopic: function(){
      var topics = App.taxonomies.topics.getContents();
      var $select = this.$el.find('.topic-select');
      var topicId = $select.find('option:selected').val();
      var topicTerm = App.taxonomies.topics.findByTid(topicId);
      this.hideToolTip();
      if(topicTerm.not_covered_in_research == 1)
      {
        $('#notCoveredModal').fadeIn('fast');
        this.hideCountriesMenu();
        this.parent.leaflet.resetColors();
        this.parent.key.hide();
      }
      else {
        console.log(topicTerm);
        this.parent.loadTopic(topicId);
        ga('send', 'event', {
          'eventCategory': 'Topic ' + topicTerm.name,
          'eventAction': 'Selected'
        });
      }
    },

    getCurrentTopic: function(){
      var topics = App.taxonomies.topics.getContents();
      var $select = this.$el.find('.topic-select');
      var topicId = $select.find('option:selected').val();
      var topicTerm = App.taxonomies.topics.findByTid(topicId);
      return topicTerm;
    },

    render: function() {
      var countries = App.taxonomies.countries.getContents();
      var topics = App.taxonomies.topics.getContents();

      var values = {countries: countries, topics: topics};
      this.$el.append($$({
      name: 'mapSelects',
      content: selects_t,
      }, values));

      this.$countrySelect = this.$el.find('.country-select');
      this.$topicSelect = this.$el.find('.topic-select');

      this.$countrySelect.select2(this.select2Options);
      this.$topicSelect.select2(this.select2Options);  

      // Bind the topic select2 open event to modify how elements are disabled
      this.$topicSelect.bind('select2:open', function(evt) {
        var $s2dropdown = $(this).data().select2.$dropdown;
        var $disabledResults = $s2dropdown.find('.select2-results .select2-results__option[aria-disabled=true]');
        $disabledResults
          .attr('aria-disabled', 'false')
          .attr('aria-selected', 'false')
          .addClass('not-covered');
      });

      this.hideCountriesMenu();
      this.hideFixedHeader();
    },

    hideFixedHeader: function() {
      //$('.fixed-header').hide();
    },

    hideCountriesMenu: function() {
      this.$el.find('.country-select + .select2').hide();
    },

    showCountriesMenu: function() {
      this.$el.find('.country-select + .select2').show();
    },

    updateTopicSelection: function() {
      this.hideToolTip();
      if(typeof(this.parent.currentTopic) == 'undefined') return;

      var topicId = this.parent.currentTopic.get('id');
      if(this.$topicSelect.val() != topicId) {
        this.$topicSelect.val(topicId);
        this.$topicSelect.find('option[value='+ topicId + ']').attr('selected', 'selected');
        this.$topicSelect.select2(this.select2Options);
        this.hideToolTip();
      }
    },
    clearCountry: function() {
      this.$el.find('.country-select').val(-1);
      this.$el.find('.country-select option[value=-1]').attr('selected', 'selected');
      this.$el.find('.country-select').select2(this.select2Options);
    },
    clearTopic: function() {
      this.$el.find('.topic-select').val(-1);
      this.$el.find('.topic-select option[value=-1]').attr('selected', 'selected');
      this.$el.find('.topic-select').select2(this.select2Options);
    },
    closeModal: function() {
      $('#notCoveredModal').fadeOut('fast');
    },
    toggleArrow: function(evt){
      $(evt.target).toggleClass('no-radius');
    },
    clearMap: function() {
      this.parent.clearAll();
    }
  });

  return SelectsView;
});
