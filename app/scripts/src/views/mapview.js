define([
  'jquery',
  
  'topic',
  'country',
  
  'baseview',

  'keyview',
  'countrycardview',
  'maphelpview',
  'mapselectsview',
  'mapzoomview',
  'leafletview',

  'text!data/countries.json',
  'mainview'
],

function($, Topic, Country, BaseView, KeyView, CountryCardView, MapHelpView, MapSelectsView, MapZoomView, LeafletView, countries_data) {
  var MapView = BaseView.extend({
    tagName: 'div',
    className: 'map',

    events: {
    },

    initialize: function() {
      CountryModel = Country;
      this.topics = {};

      this.leaflet = new LeafletView;
      this.key = new KeyView;
      this.countryCard = new CountryCardView;
      this.help = new MapHelpView;
      this.selects = new MapSelectsView;
      // TODO: either complete or remove the zoom view
      //this.zoom = new MapZoomView;

      this.key.parent = this;
      this.countryCard.parent = this;
      this.help.parent = this;
      this.selects.parent = this;
      this.leaflet.parent = this;
    },

    render: function() {
      if(this.rendered) {
        return false;
      }

      this.$el.append(this.leaflet.$el);
      this.$el.append(this.key.$el);
      this.$el.append(this.countryCard.$el);
      this.$el.append(this.help.$el);
      this.$el.append(this.selects.$el);
      // TODO: either complete or remove the zoom view
      //this.$el.append(this.zoom.$el);

      this.countryCard.render();
      this.help.render();
      this.selects.render();
      // TODO: either complete or remove the zoom view
      //this.zoom.render();
      this.leaflet.render();
      this.rendered = true;
    },

    loadTopic: function(topicId) {
      if(topicId == -1) {
        this.clearAll();
        return;
      }
      var map = this;
      if(typeof(this.topics[topicId]) == 'undefined') {
        var topic = new Topic({id: topicId});
        topic.load(function(err, model) {
          if(err) {
            console.error(err);
          }
          else {
            map.topics[topicId] = model;
            map.currentTopic = model;

            map.visualizeTopic();
            map.key.render();
            map.countryCard.hide();
            map.setRoute();
            map.selects.updateTopicSelection();
            map.selects.showCountriesMenu();
            map.has('selection');
          }
        });
      }
      else {
        map.currentTopic = this.topics[topicId];
        map.visualizeTopic();
        map.key.render();
        map.countryCard.hide();
        map.setRoute();
        map.selects.updateTopicSelection();
        map.selects.showCountriesMenu();
        map.has('selection');
      }
    },

    clearTopic: function() {
      this.selects.hideCountriesMenu();
      this.selects.clearTopic();
      this.leaflet.resetColors();
      this.key.hide();
      delete this.currentTopic;
    },

    setRoute: function() {
      if(typeof(this.currentTopic) != 'undefined') {
        var id = this.currentTopic.get('id');
        var name = this.currentTopic.get('name');

        App.router.navigate('map/' + id + '/' + escape(name));
      }
      else {
        App.router.navigate('map');
      }
    },

    visualizeTopic: function() {
      var countries = this.currentTopic.get('countries');
      this.leaflet.colorCountries(countries);
    },

    highlightCountry: function(countryCode) {
      if(countryCode == -1) {
        this.lowlightCountry();
        return;
      }

      this.countryCard.showCountry(countryCode);
    },

    lowlightCountry: function() {
      this.countryCard.hide();
      this.selects.clearCountry();
    },

    openCountry: function(countryId) {
      this.parent.openCountry(countryId);
    },

    clearAll: function() {
      // Close country card
      this.countryCard.hide();

      // Clear country selection
      this.clearCountry();

      // Clear loaded topic
      this.clearTopic();

      // Reset the URL to the map base
      this.setRoute();

      // Reset the map zoom
      this.leaflet.resetZoom();

      // Remove the selection class
      this.hasnt('selection');
    },

    clearCountry: function() {
      this.selects.clearCountry();
    },

    show: function() {
      MapView.__super__.show.apply(this, arguments);
      this.leaflet.invalidateSize();
    },

    compare: function(a,b) {
      if (a['country']['name']< b['country']['name'])
        return -1;
      if (a['country']['name'] > b['country']['name'])
        return 1;
      return 0;
    },

  });

  return MapView;
});
