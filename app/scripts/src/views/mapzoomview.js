define([
  'backbone',
  'jquery',
  'baseview',

  'text!templates/map_zoom.html'
],

function(Backbone, $, BaseView, map_zoom_t) {
  var ZoomView = BaseView.extend({
    tagName: 'div',
    className: 'map__zoom',

    events: {
    },
    initialize: function() {
    },

    render: function() {
      this.$el.empty().append(map_zoom_t);
    }
  });

  return ZoomView;
});
