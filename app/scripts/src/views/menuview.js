define([
  'backbone',
  'jquery',
  'auth',

  'text!templates/menu.html'
],

function(Backbone, $, Auth, menu_t) {
  var MenuView = Backbone.View.extend({
    tagName: 'nav',
    className: 'menu',

    events: {
      'click a': 'closeMenu',
    },

    initialize: function() {
      this.render();
    },

    render: function() {
      this.$el.append(menu_t);
    },

    closeMenu: function() {
      this.parent.toggleMenu();
    },

    switchAuth: function(authStatus) {
      // Switch from anonymous and logged in statuses
      // and display the links that should be available
      // for the current user
    }
  });

  return MenuView;
});
