define([
  'backbone',
  'jquery',
  'baseview',
  'page',

  'text!templates/loading.html',
  'text!templates/page.html',
  'text!templates/page_error.html'
],

function(Backbone, $, BaseView, PageModel, loading_t, page_t, page_error_t) {
  var PageView = BaseView.extend({
    tagName: 'div',
    className: 'page-wrapper',

    events: {
    },
    initialize: function() {
      this.models = {};
    },
    showPage: function(pageName) {
      var page = this;
      this.$el.empty()
          .append(loading_t);

      if(typeof(this.models[pageName] == 'undefined')) {
        var model = new PageModel({id: pageName});
        model.load(function(err, model) {
          if(err) {
            page.$el.empty().append(page_error_t);
          }
          else {
            ga('send', 'event', {
              'eventCategory': 'Page ' + Backbone.history.getFragment(),
              'eventAction': 'Viewed'
            });
            page.models[pageName] = model;
            page.currentModel = model;

            page.renderCurrentModel();
          }
        });
      }
      else {
        this.currentModel = this.models[pageName];
        this.renderCurrentModel();
      }
    },
    renderCurrentModel: function() {
      var pageContents = {
        title: this.currentModel.get('title'),
        body: this.currentModel.get('body')
      };
      this.$el.empty().append($$({
        name: 'page',
        content: page_t
      }, pageContents));

      this.$el.find('.page').addClass(this.currentModel.get('id'));
    }
  });

  return PageView;
});
